---
layout: default
title: Sources
language: fr
handle: /images/sources
---

Voici la liste des sources des images utilisées sur le site.

## Bannière
- [v1]({{ site.baseurl }}/images/banniereV1.png) **Sedeto** - [sedeto.fr](http://sedeto.fr)

## Entre potes
- [1]({{ site.baseurl }}/images/potes/potes1.jpg) - **La Traversée du Temps** - © MADHOUSE
- [2]({{ site.baseurl }}/images/potes/potes2.jpg) - **La Traversée du Temps** - © MADHOUSE
- [3]({{ site.baseurl }}/images/potes/potes3.jpg) - **La Traversée du Temps** - © MADHOUSE
- [4]({{ site.baseurl }}/images/potes/potes4.jpg) - **NozokiAna** - © Studio Fantasia
- [5]({{ site.baseurl }}/images/potes/potes5.jpg) - **Lucky Star** - © Kagami Yoshimizu
- [6]({{ site.baseurl }}/images/potes/potes6.jpg) - **Lucky Star** - © Kagami Yoshimizu
- [7]({{ site.baseurl }}/images/potes/potes7.jpg) - **Hibike! Euphonium** - [© Sawada Sanae](https://yande.re/post/show/374247)
- [8]({{ site.baseurl }}/images/potes/potes8.jpg) - **Vocaloid** - © [arieko](http://ecomimi.com/)
- [9]({{ site.baseurl }}/images/potes/potes9.jpg) - **Kill la Kill** - © [umakatsuhai](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=40102850)
- [10]({{ site.baseurl }}/images/potes/potes10.jpg) - **Imouto! Umaru-chan** - © [hijikini](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=52793398)
- [11]({{ site.baseurl }}/images/potes/potes11.jpg) - **Kantai Collection** - © [Yamato Tachibana](http://seiga.nicovideo.jp/seiga/im4202349)
- [12]({{ site.baseurl }}/images/potes/potes12.jpg) - **Kill la Kill** - © [azinori123](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=45304265)
- [13]({{ site.baseurl }}/images/potes/potes13.jpg) - **Precure!** - © [Yorudo Kaoru](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=43362819)
- [14]({{ site.baseurl }}/images/potes/potes14.jpg) - **Bleach** - © Tite Kubo
- [15]({{ site.baseurl }}/images/potes/potes15.jpg) - **Soul Eater** - © Atsushi Ôkubo
- [16]({{ site.baseurl }}/images/potes/potes16.jpg) - **Saki** - © Sasaki Masakatsu
- [17]({{ site.baseurl }}/images/potes/potes17.jpg) - **Shôjo Kakumei Utena** - © Chiho Saito
- [18]({{ site.baseurl }}/images/potes/potes18.jpg) - **Kannagi** - © Yôko Takata
- [19]({{ site.baseurl }}/images/potes/potes19.jpg) - **Isekai Quartet** - © Kadokawa

## Avec du public
- [1]({{ site.baseurl }}/images/public/public1.jpg) - **La Mélancolie de Haruhi Suzumiya** - © Noizi Ito
- [2]({{ site.baseurl }}/images/public/public2.jpg) - **La Mélancolie de Haruhi Suzumiya** - © Noizi Ito
- [3]({{ site.baseurl }}/images/public/public3.jpg) - **K-On!** - © Kakifly
- [4]({{ site.baseurl }}/images/public/public4.jpg) - **K-On!** - © Kakifly
- [5]({{ site.baseurl }}/images/public/public5.jpg) - **AKB0048** - © SATELIGHT
- [6]({{ site.baseurl }}/images/public/public6.jpg) - **Aibou** - © [madoka_(potechoco)](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=10021897)
- [7]({{ site.baseurl }}/images/public/public7.jpg) - **Touhou** - © [aramoneeze](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=34892413)
- [8]({{ site.baseurl }}/images/public/public8.jpg) - **Kill la Kill** - © [yosui](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=43914747)
- [9]({{ site.baseurl }}/images/public/public9.jpg) - **Naruto** - © [oba-min](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=44231171)
- [10]({{ site.baseurl }}/images/public/public10.jpg) - **Shingeki no Kyojin** - © [maedatomoko](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=42307077)

## Développé avec passion
- [1]({{ site.baseurl }}/images/passion/passion1.jpg) - **Otaku no Video** - © Gainax
- [2]({{ site.baseurl }}/images/passion/passion2.jpg) - **Otaku no Video** - © Gainax
- [3]({{ site.baseurl }}/images/passion/passion3.jpg) - **Otaku no Video** - © Gainax
- [4]({{ site.baseurl }}/images/passion/passion4.jpg) - **Tengen Toppa Gurren-Lagann** - © Gainax
- [5]({{ site.baseurl }}/images/passion/passion5.jpg) - **Ensemble stars!** - © [kaminosaki1](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=62531305)

## Logos
- [Twitter]({{ site.baseurl }}/images/social/twitterlogo.png) - © Twitter
- [Mastodon]({{ site.baseurl }}/images/social/mastodonlogo.png) - © Mastodon
- [Discord]({{ site.baseurl }}/images/social/discordapplogo.png) - © Discord

## GIF
**Pages** :
- [Fonctionnalités]({{ site.baseurl }}/images/pages/fonctionnalites.gif) - Série **Pokémon**
- [Participer]({{ site.baseurl }}/images/pages/participer.gif) - Série **Monogatari Series**
- [Nous contacter]({{ site.baseurl }}/images/pages/contacter.gif) - Série **Nichijou**
- [Télécharger]({{ site.baseurl }}/images/pages/telecharger.gif) - Série **Hyouka**
- [Donation]({{ site.baseurl }}/images/pages/donation.gif) - Série **K-On!**

**Articles** :
- [18/09/2017]({{ site.baseurl }}/images/articles/viochl.gif) - Série **Hyouka**
- [29/09/2017]({{ site.baseurl }}/images/articles/1300962431128.gif) - Série **La mélancolie de Harui Suzumiya**
- [07/11/2017]({{ site.baseurl }}/images/articles/25fded9468907.gif) - Série **Shuumatsu no Izetta**
- [12/12/2017]({{ site.baseurl }}/images/articles/841213561.gif) - Série **Hyouka**
- [18/04/2018]({{ site.baseurl }}/images/articles/mW1cgqEB.gif) - Série **Gabriel Dropout**
- [03/05/2018]({{ site.baseurl }}/images/articles/mv0b2zf4FI1.gif) - Série **Aiura**
- [16/05/2018]({{ site.baseurl }}/images/articles/15687940.gif) - Série **Sora Yori moo toi Basho**
- [25/05/2018]({{ site.baseurl }}/images/articles/ha1te98.gif) - Série **One Punch Man**
- [05/06/2018 .1]({{ site.baseurl }}/images/articles/ndupu4EZYk1.gif) - Série **La mélancolie de Harui Suzumiya**
- [05/06/2018 .2]({{ site.baseurl }}/images/articles/hea178t.gif) - Série **Gintama**
- [19/06/2018]({{ site.baseurl }}/images/articles/d254f8f2af2fa06070e4.gif) - Série **La mélancolie de Harui Suzumiya**
- [03/07/2018]({{ site.baseurl }}/images/articles/857df2f0032e2f.gif) - Série **Sora Yori moo toi Basho**
- [16/07/2018]({{ site.baseurl }}/images/articles/heaa.gif) - Série **Sora Yori moo toi Basho**
- [07/08/2018]({{ site.baseurl }}/images/articles/ty1b87K.gif) - Série **Sword Art Online**
- [14/08/2018]({{ site.baseurl }}/images/articles/1hz98rh.gif) - Série **Ano Natsu de Matteru**
- [22/08/2018]({{ site.baseurl }}/images/articles/htae1894rz.gif) - Série **K-On!**
- [03/09/2018]({{ site.baseurl }}/images/articles/Iwasawa-2575096.gif) - Série **Aiura**
- [08/11/2018]({{ site.baseurl }}/images/articles/juguagcb.gif) - Série **Yozakura Quartet**
- [28/11/2018]({{ site.baseurl }}/images/articles/00d.gif) - Série **Kokkoku**
- [13/12/2018]({{ site.baseurl }}/images/articles/juazfg.gif) - Série **Zombieland Saga**
- [14/12/2018]({{ site.baseurl }}/images/articles/kifaevtac.gif) - Série **Irozuku Sekai no Ashita kara**
- [30/04/2019]({{ site.baseurl }}/images/articles/ngioaezb.gif) - Série **K-On!**
- [06/05/2019]({{ site.baseurl }}/images/articles/bb07943896b4bae2a54325ba8c0ccf74a46d8d6dr1-500-281_hq.gif) - Série **Seishun Buta Yarou wa Bunny Girl-senpai no Yume wo Minai**
- [06/05/2019]({{ site.baseurl }}/images/articles/1hare89.gif) - Série **Princess Principal**
- [30/06/2019]({{ site.baseurl }}/images/articles/ozh8d32Dhr1tx45yjo1.gif) - Série **Himouto! Umaru-chan**
- [24/07/2019]({{ site.baseurl }}/images/articles/tnn18azh.gif) - Série **Kore Wa Zombie Desu Ka**
- [19/11/2019]({{ site.baseurl }}/images/articles/o65ucdhnaJ1vp3jsgo10.gif) - Série - **Sword Art Online**
- [29/11/2019]({{ site.baseurl }}/images/articles/3294445142.gif) - Série **Violet Evergarden**
- [16/12/2019]({{ site.baseurl }}/images/articles/mvoexxKErq1spnzlpo1_500.gif) - Série **Higurashi no naku koro ni**
- [10/01/2020]({{ site.baseurl }}/images/articles/ngzoiu.gif) - Série **Hinamatsuri**
- [22/01/2020]({{ site.baseurl }}/images/articles/HoarseFastAuk-size.gif) - Série **K-Project**
- [04/03/2020]({{ site.baseurl }}/images/articles/c72b3fbb.gif) - Série **ZombieLand Saga**
- [12/03/2020]({{ site.baseurl }}/images/articles/beaufi448a.gif) - Série **Hibike! Euphonium**
- [19/03/2020]({{ site.baseurl }}/images/articles/gjdhezhse.gif) - Série **Mondaiji-tachi ga Isekai kara Kurusou Desu yo?**
- [04/04/2020]({{ site.baseurl }}/images/articles/1400166608871.gif) - Série **Nisemonogatari**
- [01/05/2020]({{ site.baseurl }}/images/articles/n88c1zvI8G1rcw.gif) - Série **Shinsekai Yori**
- [23/05/2020]({{ site.baseurl }}/images/articles/joshiraku.gif) - Série **Joshiraku**
- [15/07/2020]({{ site.baseurl }}/images/articles/oun98bhlM11wt5xqro1.gif) - Série **Owarimonogatari**
- [17/08/2020]({{ site.baseurl }}/images/articles/z1hr9h1rz9.gif) - Série **Owarimonogatari**
- [15/04/2021]({{ site.baseurl }}/images/articles/kizuupkissshot_v3.gif) - Série **Kizumonogatari**
- [17/06/2021]({{ site.baseurl }}/images/articles/19hzr8q1hrz.gif) - Série **Zombieland Saga**
- [16/11/2021]({{ site.baseurl }}/images/articles/1hr8z9.gif) - Série **Kyoukai no Kanata**
- [12/01/2022]({{ site.baseurl }}/images/articles/vjCVpWG.gif) - Série **Quintessential Quintuplets**
- [27/03/2022]({{ site.baseurl }}/images/articles/bd4.gif) - Série **Kobayashi-san Chi no Maidragon**
- [31/10/2022]({{ site.baseurl }}/images/articles/1517419327.gif) - Série **Natsu-iro Kiseki**
- [14/06/2023]({{ site.baseurl }}/images/articles/1398596098324.gif) - Série **Kyoukai no Kanata**

## Divers
- [Présentation]({{ site.baseurl }}/images/presentation.png) **Kmeuh**
- Les captures d'écran des interfaces de **Karaoke Mugen** présentes sur le site sont libres de droits.