Lorsque vous ajoutez des images dans les dossiers "public" et "potes", veuillez veiller que le ratio longueur/largeur soit d'environ 1.75.
Ceci est par souci d'esthétique, afin d'éviter que les textes ne soient pas côte à côte.

//

If you want to add pictures in "public" or "potes" (buddies) folders, please make sur the height/width ratio is around 1.75.
This is a matter of design, making sur not to mess up the displayed texts around those pictures.
