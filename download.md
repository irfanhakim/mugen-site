---
layout: default
title: Télécharger Karaoke Mugen
language: fr
handle: /download.html
---

![gif]({{ site.baseurl }}/images/pages/telecharger.gif){: .center-image }

## Téléchargement

### Versions stables

#### v{{ site.data.releases.channels.release.version }} « *{{ site.data.releases.channels.release.name }}* » ({{ site.data.releases.channels.release.date }})

##### Windows

- [Installeur]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.release.fileSuffix }}.exe) **recommandé**
- [.zip portable]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-win.zip)

##### macOS

###### Processeurs Intel

- [Image .dmg]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-x64.dmg) **recommandé**
- [Archive Zip]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-x64.zip)

###### Processeurs Apple Silicon

- [Image .dmg]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-arm64.dmg) **recommandé**
- [Archive Zip]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-arm64.zip)

##### Linux

- [Flatpak](https://flathub.org/apps/details/moe.karaokes.mugen) **recommandé**
- [AppImage x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-x86_64.AppImage)
- [AppImage arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.AppImage)
- [Ubuntu/Debian x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-amd64.deb)
- [Ubuntu/Debian arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.deb)
- [Archive .tar.gz x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-x64.tar.gz)
- [Archive .tar.gz arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.tar.gz)

###### Pré-requis système

**Les binaires Linux ont besoin d'une version 2.31 ou plus de la GLIBC**

Par exemple sur Ubuntu, cette GLIBC n'est présente que sur les versions 20.04 et plus.

## Installation

[Consultez la documentation d'installation !](https://docs.karaokes.moe/fr)

## Mises à jour sur Windows

### Si vous voulez mettre à jour une version portable

Si Karaoke Mugen est déjà téléchargé et que vous mettez à jour vers une nouvelle version, décompressez simplement l'archive dans le même dossier, en remplaçant les anciens fichiers par les nouveaux. **La version portable n'est pas compatible avec la mise à jour automatique.**

## Versions expérimentales

- Version `master` {{ site.data.releases.channels.master.version }} « *{{ site.data.releases.channels.master.name }}* » ({{ site.data.releases.channels.master.date }}) : bugfixes et légères modifications. Considérée stable.
  - [Windows (Installer)]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.master.fileSuffix }}.exe)
  - [Windows (portable)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-win.zip)
  - [macOS (Image DMG pour Mac Intel)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-x64.dmg)
  - [macOS (Image DMG pour Mac Apple Silicon (arm64))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-arm64.dmg)
  - [macOS (Archive ZIP pour Mac Intel)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-x64.zip)
  - [macOS (Archive ZIP pour Mac Apple Silicon (arm64))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-arm64.zip)
  - [Linux (AppImage x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-x86_64.AppImage)
  - [Linux (AppImage arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.AppImage)
  - [Linux (AUR)](https://aur.archlinux.org/packages/karaokemugen-git/)
  - [Linux (.deb x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-amd64.deb)
  - [Linux (.deb arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.deb)
  - [Linux (.tar.gz x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-x64.tar.gz)
  - [Linux (.tar.gz arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.tar.gz)
- Version `next` {{ site.data.releases.channels.next.version }} « *{{ site.data.releases.channels.next.name }}* » ({{ site.data.releases.channels.next.date }}) : version de développement, nouvelles fonctionnalités des futures versions.
  - [Windows (Installer)]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.next.fileSuffix }}.exe)
  - [Windows (portable)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-win.zip)
  - [macOS (Image DMG pour Mac Intel)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-x64.dmg)
  - [macOS (Image DMG pour Mac Apple Silicon (arm64))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-arm64.dmg)
  - [macOS (Archive ZIP pour Mac Intel)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-x64.zip)
  - [macOS (Archive ZIP pour Mac Apple Silicon (arm64))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-arm64.zip)
  - [Linux (AppImage x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-x86_64.AppImage)
  - [Linux (AppImage arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.AppImage)
  - [Linux (AUR)](https://aur.archlinux.org/packages/karaokemugen-git/)
  - [Linux (.deb x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-amd64.deb)
  - [Linux (.deb arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.deb)
  - [Linux (.tar.gz x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-x64.tar.gz)
  - [Linux (.tar.gz arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.tar.gz)

## Code source

Le code source est disponible pour chaque version et branche [sur la page du dépôt git](https://gitlab.com/karaokemugen/code/karaokemugen-app).

## Versions précédentes

[Consultez la page du dépôt git réservée aux anciennes versions](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags).

D'anciennes versions binaires sont encore disponibles [ici](https://mugen.karaokes.moe/downloads). Généralement, la dernière version mineure de chaque version majeure est conservée.
