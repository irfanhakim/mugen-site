---
layout: post
title:  "New release: Karaoke Mugen 3.1.2 !"
date:   2020-03-12 06:00:00
language: en
handle: /2020/03/12/KM312.html
---

Karaoke Mugen 3.1.2 "Mitsuha Matinale" can't believe it !

![gif]({{ site.baseurl }}/images/articles/beaufi448a.gif){: .center-image }

This is a minor release but contains a few new features. Check the changelog below !

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/blob/v3.1.2/CHANGELOG.md)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
