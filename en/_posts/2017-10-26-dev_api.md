---
layout: post
title:  "[DEV] Full API documentation"
date:   2017-10-26 09:00:00
language: en
handle: /2017/10/26/dev_api.html
---

Hi all ! Axel here, I'm the lead developper of **Karaoke Mugen**.

Perhaps you didn't know, but **Karaoke Mugen** has a REST API which you can use to interact in various ways on **KM** :

* List karaokes and their respective metadata
* Manage playlists
* Manage video player
* etc.

Basically, the web interface uses this API to interact with the engine but what this *means* is ... You're free to make your own **KM** client : Swift, C, Java, Go, Ruby, whatever YOU want !

An extensive documentation was written to allow anyone developping around our engine. [You'll find it here.](https://mugen.karaokes.moe/apidoc).

Your feedback is very welcome, as usual, but if you think you can help us improving **Karaoke Mugen** in any way, feel free to come and share with us on [Discord](https://karaokes.moe/discord) !
