---
layout: post
title:  "Karaoke Mugen Versions"
date:   2017-11-14 10:00:00
language: en
handle: /2017/11/14/versions_km.html
---

We've rewritten the download page to display each link by OS, rather than by version.

Two new versions have also appeared, so let's introduce them :

- Numbered versions (2.0, 2.0 beta, 2.1 ...) are **stable** versions, and should run fine in theory. They are the recommended versions.

- `Master` is updated with each version we create. Most fixes will be present here before the last numbered version available. It's kind of a « Stable but we won't say so too loud » version.

- `Next` is the experimental, nightmare mode, ultimate challenge : those are **Karaoke Mugen** versions with functionnalities not tested thoroughly, and potentially with multiple bugs, but that will allow you to taste all new flavors for the application. As for example, at the same time I'm writing this post, `next` version features a 15 seconds preview for karas, so you can be sure this is the song you're thinking about before you submit it!

`Master` et `Next` are updated daily. If you use the executable version or manually download the source, you will have to download them again each time. If you want to stay up-to-date, you should consider using Git.
