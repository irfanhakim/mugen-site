---
layout: post
title:  "Karaoke Mugen at Japan Expo Sud"
date:   2018-03-05 10:00:00
language: en
handle: /2018/03/05/japan_expo_sud.html
---

Just a brief news to say [Forum Thalie](http://forum-thalie.fr) will be present at [Japan Expo Sud](http://www.japan-expo-sud.com/fr/) having an activity stand. Amongst these, you guessed it, a karaoke activity relying on **Karaoke Mugen**.

This will be the first time **Karaoke Mugen** is used in a convention. Even tho it's on version 2.0.7, which is quite stable and well-tested, this is quite a victory for the project.

Feel free to go and see them on stand **P092**! They also have a lot of activities for your entertainment, other than karaoke. More info on [this tweet](https://twitter.com/ForumThalie/status/969527486189989888)
