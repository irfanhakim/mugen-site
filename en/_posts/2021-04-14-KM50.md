---
layout: post
title:  "Karaoke Mugen 5.0!"
date:   2021-04-14 06:00:00
language: en
handle: /2021/04/14/KM50.html
---

Karaoke Mugen 5.0 « Poppy Partagée » is hesitant!

This is a major version, with a lot of new features. Make sure to read the changelog.

![gif]({{ site.baseurl }}/images/articles/kizuupkissshot_v3.gif){: .center-image }

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/tags/v5.0.31)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
