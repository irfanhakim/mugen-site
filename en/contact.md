---
layout: default
title: Contact us
language: en
handle: /contact.html
---

![gif]({{ site.baseurl }}/images/pages/contacter.gif){: .center-image }

If you have any questions or general requests, don't hesitate.

For bugs and other troubles with Karaoke Mugen, please note we have two separate issue management systems for **Karaoke Mugen** :

- [Bugs concerning the actual Karaoke Mugen application](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues)
- [Bugs concerning the karaoke database](https://gitlab.com/karaokemugen/bases/karaokebase/issues)

If you encounter any problem, please check another similar issue has not already been submitted. You can then open a new one!

## Mail

That's easy : mugen at karaokes.moe

## 𝕏 (formerly Twitter)

That's easy as well : [@karaokemugen](https://twitter.com/karaokemugen).

# Bluesky

A little less easy but it's there : [@karaoke.moe](https://bsky.app/profile/karaokes.moe).

## Mastodon

Follow [@KaraokeMugen@shelter.moe](https://shelter.moe/@KaraokeMugen)!

## Discord

On Discord, you'll find us on this [server](https://karaokes.moe/discord)!