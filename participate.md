---
layout: default
title: Participer à l'expansion du karaoké
language: fr
handle: /participate.html
---

![gif]({{ site.baseurl }}/images/pages/participer.gif){: .center-image }

## La communauté du kara !

Pour rejoindre la communauté, poser des questions, avoir des réponses, et tout ça dans la bonne humeur, il y a deux possibilités :

- [Notre forum Discourse](https://discourse.karaokes.moe)
- [Notre serveur Discord](https://karaokes.moe/discord)

Utilisez le support de votre choix pour dialoguer avec la communauté !

## Aider Karaoke Mugen

Selon ce que vous savez faire, et surtout ce que vous aimeriez faire, on pourrait avoir besoin de vos compétences.

**Karaoke Mugen est un projet libre**, chacun peut y participer et y apporter sa pierre. Cela peut être pour vous une bonne occasion de :

- Travailler en groupe.
- Apporter vos propres compétences : design, sous-titrage, traduction...
- Pour les informaticiens en herbe, apprendre à maitriser git, NodeJS, React, Aegisub, l'encodage vidéo, HTML, CSS, JS...
- Faire valoir sur votre CV que vous avez participé à un logiciel ou un projet libre (ne riez pas, c'est une expérience intéressante et appréciée !).
- Faire connaissance avec des gens, rencontrer de nouvelles têtes.
- **Le plus important, s'amuser et faire quelque chose tous ensemble !**

Dans tous les cas, si vous avez des questions, n'hésitez pas à visiter la page [contact]({{ site.baseurl }}/contact.html).

Voici une liste des tâches sur lesquelles on pourrait avoir besoin d'un coup de main :

## La base de karaokés

- **Créez vos propres karaokés** : Voir des nouveaux titres, être surpris par les choix (et les goûts de certains) c'est ça aussi le karaoké. Vous avez une chanson totalement improbable en tête, un kara que vous seul chanterez, mais c'est pas grave, ou bien vous avez remarqué que la base manquait cruellement de génériques de Bleach (spoiler : c'est faux), alors [foncez découvrir notre tutoriel sur comment réaliser des karaokés !](https://docs.karaokes.moe/fr/contrib-guide/create-karaoke)
- **Réparer des karaokés mal *timés*** : Y'en a dans la base, ce n'est pas un secret vu qu'on [les répertorie quand on les trouve](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=lyrics) mais on n'a pas toujours le temps de s'en occuper rapidement. Il faut savoir utiliser Aegisub.
- **Le remplacement des vidéos par de la meilleure qualité** : Beaucoup de nos vidéos sont en qualité standard voire médiocre. Nous avons [un GDocs pour suivre tout ça (presque) correctement](https://docs.google.com/spreadsheets/d/1tYGKx4MeKXu5eNValQq3kfX3wkrIXz5H3zyG9bAsaSA/edit#gid=0). Nous possédons beaucoup de vidéos de haute qualité prêtes à être intégrées, mais cela prend un peu de temps, car il faut vérifier que les paroles ne se retrouvent pas décalées. C'est une tâche de fond que nous effectuons de temps en temps sur certaines vidéos, mais il en reste environ 300 ! Il faut savoir encoder des vidéos.
- **Il nous manque des informations sur certains karaokés** comme le chanteur, le compositeur, ou le titre. Parfois il s'agit d'obscures AMV ou d'autres vidéos où nous avons du mal à trouver les informations. [La liste des karas sans information est tenue à jour ici](https://gitlab.com/karaokemugen/bases/karaokebase/issues/417). Il faut connaître les bonnes adresses et savoir chercher.
- **Des jingles !** Nock, Stal et Nabashi en ont [déjà réalisé pas mal](https://gitlab.com/karaokemugen/medias/jingles), mais si l'envie vous prend de réaliser des choses rigolotes qui sont diffusées comme petits intermèdes entre les chansons, alors faites-vous plaisir ! Montage vidéo, animation, c'est comme vous le sentez !
- **Des intros !** Kmeuh [a ouvert le bal](https://gitlab.com/karaokemugen/medias/intros) en réalisant quelques vidéos de démarrage de session karaoké, reprenant des intros déjà existantes dans le vrai monde réel de la réalité véritable. Vous êtes les bienvenus pour en proposer d'autres !
- **Exaucer les souhaits des utilisateurs !** On nous fait régulièrement [des suggestions de karaokés à créer](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=suggestion), mais on n'a pas le temps de tout faire... Si vous avez un peu de temps et l'envie de *timer* (ou apprendre à) des karaokés pour faire plaisir à ceux qui en demandent, alors venez nous voir !

## Développement

Il y a pas mal de petits projets à droite à gauche sur lesquels on peut avoir besoin d'aide :

- **L'application Karaoke Mugen a une flopée de tickets ouverts qu'il faut résoudre**. [Vous pouvez voir la liste ici](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues). Certains demandent de savoir manipuler React, d'autres juste NodeJS, d'autres de connaître les bases de données PostgreSQL ou encore faire de la sorcellerie avec ffmpeg/mpv.
- **Un serveur Karaoke Mugen a été développé**. Il permet de fournir un raccourcisseur d'URL pour vos sessions de karaoké, et est déjà utilisé comme base de téléchargement pour vos chansons ou gérer les comptes en ligne et les stats de façon unifiée. [Le projet est mature, mais il y a des fonctionnalités qu'on aimerait encore ajouter](https://gitlab.com/karaokemugen/code/karaokemugen-server) et ne demande que vos compétences en NodeJS, Nuxt et PostgreSQL. Par exemple pour améliorer [la liste des karaokés](https://kara.moe).
- **Karaoke Mugen est disponible pour votre Raspberry Pi !** Xefir a [mis à disposition](https://gitlab.com/karaokemugen/MugenPi) son travail sur le sujet, mais il y a sûrement des choses à améliorer si vous aimez bricoler sur Raspberry Pi !

## La partie web

- **Le site web sur lequel vous êtes a toujours besoin d'améliorations !** Il utilise [Jekyll](http://jekyllrb.com) pour se générer, mais sinon c'est du bête HTML/CSS/JS de base. Si vous pensez pouvoir améliorer des choses ou même passer à un autre système de génération plus puissant et plus intéressant, nous sommes ouverts à toute suggestion. En attendant [le code du site web est disponible !](https://gitlab.com/karaokemugen/sites/mugen-site)
- **La documentation de Karaoke Mugen est là, mais elle pourrait être mieux !** Meilleure mise en page, meilleure traduction en anglais, plus d'exemples, plus d'aide, plus didactique... [Les sources sont aussi disponibles ici !](https://gitlab.com/karaokemugen/sites/docs). Nous utilisons [Hugo](https://gohugo.io/) parce que c'est simple, mais il y a certainement mieux ailleurs !
- **Le Twitter Game, le Mastodon Game, le Facebook Game...** C'est mignon tout ça, mais notre présence sur les réseaux sociaux est minimale. Il y a peut-être des choses à améliorer ici aussi. Si vous êtes familiers avec ces outils et avez des idées sur ce qu'on pourrait faire, ça serait pas mal.

## D'autres idées ?

Vous avez envie de participer, mais rien de tout cela ne vous botte, pas de problème. Vous avez une idée géniale à proposer et aimeriez vous en occuper ? [Contactez-nous !]({{ site.baseurl }}/contact.html)