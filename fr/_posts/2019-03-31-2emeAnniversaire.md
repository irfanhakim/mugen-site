---
layout: post
title:  "2e anniversaire !"
date:   2019-03-25 07:00:00
language: fr
handle: /2019/03/25/2emeAnniversaire.html
---

Karaoke Mugen approche de son second anniversaire : il y a deux ans, voulant créer un logiciel de gestion de karaoké ainsi qu'une base communautaire, une petite équipe se rassemble et commence à s'organiser. Avant de commencer à taper du code et à faire des karaokés, il a fallu mettre des choses en place. Une infrastructure pour travailler ensemble, un endroit où stocker les données...

Et les données, il y en a beaucoup : aujourd'hui la base pèse plus de 215 giga-octets, avec un peu plus de 6700 karaokés. Ces karaokés, ainsi que le code de l'application, du site, de Karaoke Mugen Server, etc. sont le fruit de nombreuses personnes qui ont participé, de près ou de loin, à ce qu'est Karaoke Mugen aujourd'hui.

![gif]({{ site.baseurl }}/images/articles/03e95465.gif){: .center-image }

[Je me suis inspiré de ce billet de Kabu](http://champdenavet.free.fr/index.php?post/2018/05/22/Les-Invisibles-de-la-PASSION) actuel président de [Nijikai](http://nijikai.fr), qui montrait l'envers du décor de la création d'une convention comme [Jonetsu](http://jonetsu.fr).

Je voulais vous montrer, surtout, que Karaoke Mugen, ce sont des hommes et des femmes qui donnent de leur temps libre pour faire vivre le karaoké libre en France (et un peu aussi dans le monde, comme en Belgique par exemple). En deux ans, il y a eu tellement de choses faites qu'il serait fastidieux de toutes les citer.

Par contre, on peut regarder ensemble qui a contribué à ce projet, car ils sont nombreux, et comme vous allez voir, ils ont aidé à des degrés divers et variés.

# Mentions spéciales

## Bakaclub

Mention spéciale aussi à des membres du Bakaclub, qui de façon générale nous a souvent épaulé. Le Bakaclub est une association étudiante qui organise la convention [Bakanim'](https://bakanim.iiens.net/) et possède [une base de karaokés conséquente](https://manga.iiens.net/karas/). Non contents de la partager, ils nous ont prêté main forte sur de nombreux sujets : documentation, code, et aide générale :

- **Seipas** : a réalisé [EpitASS](https://framagit.org/Seipas/EpitASS) qui a permis de convertir une partie des karaoké en version 1 d'Epitanime. Une partie seulement car certains ont quand même dû être réécrits à la main, mais le travail de rétro-ingénieurie opéré par Seipas a été remarquable.
- **SilvoSwordo**, grand connaisseur du format de sous-titrage ASS utilisé pour les karaokés, il répond toujours présent quand on a une question très pointue sur le sujet.
- **Elniven et Sting**, qui nous ont fourni les karaokés les plus récents de la base du Bakaclub

## KuroSensei

Kuro est un chilien qui adore le karaoké et time comme un fou des séries récentes. Il a mis à disposition [sa base de karaokés sur Github](https://github.com/animekaraokesass) et comme elle est sous license Creative Commons, elle nous a permis de combler certains trous dans notre base de karaokés. Et cerise sur le gâteau, c'est un mec vraiment cool.

## Soramimi

[Soramimi Karaoke](http://soramimikaraoke.blogspot.com/) est une petite équipe des pays-bas qui a développé un logiciel de karaoké ainsi qu'une petite base de données de versions longues de chansons. Ce n'est pas toujours ce qu'on recherche (des versions longues) mais on en a quand même quelques-unes sous forme de MP3 dans la base de Karaoke Mugen.

## Epitanime

Comment ne pas citer [Epitanime](http://www.epitanime.com) quand on parle de karaoké ? Activité phare de ses membres et durant les conventions annuelles, c'est le karaoké d'Epitanime qui a donné envie à nombre d'entre nous de créer Karaoke Mugen. Sans Epitanime, il n'y aurait pas ce site sur lequel vous êtes actuellement.

Une petite moitié de la base de karaokés aujourd'hui est héritée de celle d'Epitanime, que nous avons retapée, réorganisée et convertie à nos standards de qualité vidéo.

Ce n'est sûrement pas réciproque, mais au sein de Karaoke Mugen, on aime beaucoup Epitanime et son karaoké.

# Les gens derrière Karaoke Mugen

Je ne vais pas citer l'intégralité des contributeurs : certains d'entre eux me sont assez peu connus et ne sont pas restés très longtemps. C'est le propre de l'open source : on vient, on fait une contribution, on s'en va.

Il serait néanmoins injuste de ne pas les citer : **Chaboudaba, DL21, Darckoune, herolien, seima, Joffrey Abeilard, Kyonata, Lau, Matigno, Apey, Psoushi, Rukawa, Stal, tartar lord, Thaksin**

Ces personnes ont soit corrigé un bug quelconque ou ont contribué de temps en temps avec des karaokés. Si leur contribution n'est pas aussi importante en quantité que les autres membres dont je vais vous parler, sans eux il n'y aurait par exemple pas certains karaokés dans la base. Vous assurez !

Pour les timeurs de karaokés, n'oubliez pas que [la liste est consultable dans la base de karaokés](https://kara.moe/authors?p=0&filter=&order=recent&q=&slug=)

## Aeden

Aeden est un nantais amateur de chapeaux, de fouets, et d'aventuriers luttant contre des nazis, mais c'est aussi le couteau suisse de Karaoke Mugen. Timeur, intégrateur (il prend des karaokés terminés et les intègre dans Karaoke Mugen), développeur et butler d'élite, cet homme fait tout. Il aime le rangement, et il adore les issues (bugs) surtout quand ils sont fermés. Ses faits d'armes incluent notamment le remplacement de plus de 3000 vidéos de mauvaise qualité par des versions DVD ou Blu-ray, parfois creditless, ou encore la mise en place de tests unitaires pour vérifier que chaque version de Karaoke Mugen fonctionne correctement (et qu'on a pas cassé un truc par mégarde en corrigeant un bug)

## Amo

Encore un amateur de chapeaux, mais pas de fouets. [Amo est un gars sûr de notre otakusphère](http://ranka.fr) et gère beaucoup trop de projets pour son propre bien. Il a offert de nombreux retours très intelligents sur l'application Karaoke Mugen, et a contribué avec quelques times bien sentis.

## Angor de Redjak

Fringuant jeune homme à la chevelure abondante, Angor a eu l'idée géniale, un jour, de streamer Karaoke Mugen sur Twitch. Du coup il se fait des sessions de temps en temps, et nous aide en fournissant de nouveaux karaokés mais aussi en donnant son avis. Et faudrait qu'il y ait plus de gens comme lui, parce que les retours, c'est important.

## Ava

Ava est une jeune femme de Nantes elle aussi, et qui nous a suivis quelques mois pour notamment s'occuper de l'internationalisation du [site de l'application Karaoke Mugen](https://mugen.karaokes.moe/en). Elle a fait en sorte que le site soit traduisible et l'a traduit en anglais.

## AxelTerizaki

Encore un Nantais, bon sang ils sont partout ! Je n'aime pas trop parler de moi-même, mais s'il faut bien me coller une casquette, c'est celle de chef de projet. Je m'occupe d'un peu tout, je touche à tout (sauf aux times de karaokés, que je ne maitrise pas). Quand il faut trancher, prendre des décisions, ou servir de moteur à une idée, je prends les choses en main pour pousser les autres. Ma plus grosse contribution à Karaoke Mugen, ça reste probablement l'application à installer sur son ordinateur dont j'ai écrit une grande partie du code.

## Benoît Courtine

Benoît est un développeur qui a vu en Karaoke Mugen un bon moyen de me secouer les puces pour que je m'améliore. Il a notamment aidé au nettoyage du code de l'application, et est toujours de bon conseil quand il s'agit de chercher un moyen d'optimiser les choses et gagner de précieuses milisecondes en temps de traîtement. C'est malheureusement quelqu'un de très occupé par son travail.

## Bhaal42

Bhaal nous a fait part de quelques karaokés, mais surtout de son expérience en tant que gérant de salle karaoké à Epitanime lorsqu'il en faisait partie. Cela nous a permis d'affiner les fonctionnalités que nous avons mises dans l'application Karaoke Mugen.

## Bibah

Jeune étudiant qui a du mal à faire comprendre ses blagues aux autres, il adore jouer Bastion dans Overwatch, mais surtout il a voulu aider Karaoke Mugen pour s'initier à NodeJS. C'est notamment lui qui a ajouté un shuffle intelligent et il a participé à la refonte de la configuration de l'application.

## Coyoyann

Yann adore trifouiller du matériel embarqué et faire du code de bas niveau. Il serait par exemple capable de faire chanter votre cafetière (littéralement.) Mais il a aussi une passion du karaoké et ça l'a conduit à aider Karaoke Mugen. Il a notamment fait des recherches sur les modificateurs de gain audio (ce qui permet à Karaoke Mugen de garder un volume constant en passant d'une chanson à l'autre), sur des tests de charge (pour vérifier que l'application est utilisable par 10, 100, 1000 personnes...) Il aide aussi occasionellement sur d'autres sujets liés au code de l'application Karaoke Mugen.

## Eden

Eden est arrivé un jour avec un camion rempli de karaokés Toyunda (le format utilisé à Epitanime) qu'il n'avait jamais envoyé à ceux-ci. Cela a réveillé quelques flashback du vietnam parmi les contributeurs de Karaoke Mugen ayant travaillé sur le format Toyunda, mais au final, en travaillant ensemble, on a réussi à intégrer ses quelques 350 karaokés dans la base. Lui et ses comparses sont nos fournisseurs officiels de chansons obscures d'animés de mecha d'un temps que les moins de 20 ans ne peuvent pas connaître...

## Esenjin

Notre monsieur graphiste, gifs animés et café. Esenjin (ou Sangigi) est notamment responsable de l'habillage graphique du [site web de l'application Karaoke Mugen](https://mugen.karaokes.moe), mais aussi des flyers qu'on vient de faire imprimer pour les distribuer en convention. Il est également de bon conseil quand on demande son avis aux membres de la communauté.

## Fskng

S'il n'est plus présent parmi nous depuis la version 2, il faut bien rendre à César ce qui lui appartient : Fskng a été d'une aide précieuse pour le développement de la version 1 de Karaoke Mugen. Comme NodeJS ne l'intéressait pas vraiment, il est parti vers d'autres horizons, mais son aide nous aura été précieuse aux tous débuts du projet.

## Jaerdoster

Gourou de la base de données, chevalier et chef de famille émérite, Jaerdoster a fourni les plans de l'étoile n... enfin de la base de données de Karaoke Mugen, et a aidé à coder tout ce qui écrit et lit des données dans l'application depuis les bases SQLite et maintenant PostgreSQL. Sans lui, J'aurais probablement fait des choix très douteux que j'aurais regretté plus tard. C'est donc grâce à lui que quand vous tapez « Hagane no Renkijutsushi » dans le moteur de recherche, vous tombez sur « Fullmetal Alchemist ».

## Keul

Comme Fskng, Keul a été d'une grande aide sur la version 1 de Karaoke Mugen qui était écrite en PHP. Il a aussi permis aux contributeurs de karaokés d'envoyer leurs oeuvres via un formulaire basique mais fonctionnel. Aujourd'hui il est plutôt occupé avec l'aspect technique de [Jonetsu](http://jonetsu.fr)

## Kramoule

Quand il a le temps, Kramoule adore faire un seul karaoké par mois afin de se retrouver dans le bilan que j'écris chaque fin de mois et qui liste les ajouts dans la base. Accessoirement, il a aussi contribué à éliminer certains karas en hardsub dans la base, ainsi que d'en corriger quelques uns.

## Krow

Krow, c'est quand son système n'est pas en panne qu'il nous offre des times, souvent de chansons longues et obscures que personne ne voudra vraiment chanter d'ailleurs, mais il en faut aussi. Et quand son ordi marche et qu'il ne time pas, il trouve des bugs et les décrit de façon très explicite et passionnée, afin que les développeurs de l'application les réparent dés que possible.

## Kmeuh

Certainement l'un des piliers de Karaoke Mugen grâce à son enthusiasme et sa versatilité, Kmeuh (prononcez K-Meuh pour l'énerver, à vos risques et périls, car c'est un *D.va main* sur Overwatch) fait du time, de l'intégration, mais s'est aussi fait chier à vérifier, remplir et harmoniser les noms des séries. C'est aussi grâce à lui que quand vous tapez « Hagane no Renkijutsushi » ça vous sort « Fullmetal Alchemist ». Ouais j'aime bien cet exemple.

## LordB

Un père de famille discret mais sans qui Karaoke Mugen 2.0 n'existerait probablement pas. C'est en développant un lanceur pour Karaoke Mugen 1.x qu'il a découvert NodeJS, m'en a parlé, et nous a aidés à brainstormer le squelette de l'application. Il a récemment développé [Karaoke Mugen Explorer](https://kara.moe) et taffe actuellement sur un gestionnaire de téléchargement de base de karaokés.

## Lua

Adorateur de Yu-Gi-Oh!, Kingdom Hearts et Chunibyô devant l'éternel, ce Rennais a notamment fait quelques karaokés d'anthologie comme [Mégagaf](https://live.karaokes.moe/?video=88344338-4d71-4930-9bba-bc75ecc3deaa) ou encore [Le temps de la rentrée](https://live.karaokes.moe/?video=2454ca32-e8d6-47cb-a1b3-448bde5f1f38). Il a également écrit une grande partie de [la documentation contributeur](https://docs.karaokes.moe/fr/contrib-guide/create-karaoke/) et réparé un petit paquet de karaokés défectueux. Il était également présent lors du brainstorming initial, avec Nemotaku, LordB et moi-même.

## Lukino

En suisse aussi, on aime bien Karaoke Mugen ! Lukino a contribué aux débuts du projet à améliorer les outils d'accès à la base de données de l'application. Aujourd'hui il traîne toujours dans les parages, mais se fait plus discret.

## MaxaoH

Avec Nock, MaxaoH est notre monsieur Italien. Fanboy de Giorgio Vanni (le Bernard Minet italien, pour vous situer, mais toujours en activité) il a enrichi les karaokés italiens et nous a déniché quelques perles durant ses recherches sur le sujet. C'est également un connaisseur de Kamen Rider.

## Meulahke

Notre caution Belge du groupe. Meulahke fait tourner Karaoke Mugen durant des soirées entre amis là-bas et nous a fait de nombreux retours sur l'application, mais a aussi pas mal de karaokés dans son répertoire. Parfois des choses un peu obscurs, mais comment ferait-on sans les chansons de karaoké de la série de jeux Yakuza ?

## mino

mino est, avec DyspC, arrivé récemment parmi nous. Il a développé un prototype de convertisseur de karaoké au format Karafun vers le format de sous-titre ASS utilisé par Karaoké Mugen. Peut-être trouverons-nous de quoi enrichir la base encore plus grâce à ça !

## Mirukyu

Mirukyu traîne avec Sedeto, et a contribué à Karaoke Mugen un beau jour après une session avec cette dernière. Ses deux seules contributions, mais non des moindres, aura été de documenter le fichier de configuration, et implanter un algorithme d'insertion intelligente pour que, par exemple, un karaoké soit ajouté plus tôt dans la liste de lecture si son demandeur n'a pas fait de demande depuis longtemps. Merci à lui !

## Nabashi

Cette nantaise amatrice de dinosaures se sert de son butler d'élite Aeden pour nous faire parvenir ses karaokés, la plupart d'otome game ou de Touken Ranbu, communauté avec laquelle elle a fusionné (un peu comme avec mon canapé, ça nous a pris 3 jours pour les séparer).

## nah

Si [Karaoke Mugen Live](https://live.karaokes.moe) fonctionne aussi bien, vous pouvez remercier nah, qui le bichonne, le garde à jour et corrige les bugs de time quand il en trouve. Mais ça, c'est le bon côté de nah. Sa face cachée, c'est celle qui time [des chansons des Musclés](https://live.karaokes.moe/?video=ff7b2413-e124-4c8e-8aa1-877a11bd5b45) histoire de nous rappeler les heures les plus sombres de notre histoire.

## Nock

Quand cet habitant de Laval s'investit dans un projet, il ne fait pas ça à moitié. De nature perfectionniste, là où il passe, les irrégularités trépassent. Si chaque chanson de Karaoke Mugen ou presque contient des informations précises et exactes comme son chanteur, son compositeur ou le studio qui a crée l'oeuvre, c'est grâce à Nock qui s'est tapé pratiquement toutes ces métadonnées (environ 5000 à l'époque) avec un peu d'aide de Kmeuh et de RdNetwork. C'était un travail de titan, et seul un titan pouvait s'en occuper. Notons aussi qu'il a réalisé un petit paquet de jingles rigolos pour passer entre des chansons de Karaoke Mugen et [que vous pouvez consulter ici](https://gitlab.com/karaokemugen/medias/jingles). Enfin, il serait difficile de ne pas mentionner [tous les karaokés](https://kara.moe/karas?p=0&filter=&order=search&q=t%3A159&slug=nock) qu'il a réalisés pour la base.

## Nodamin

Discrète mais particulièrement efficace, Nodamin nous a gratifié [d'une palanquée de karaokés](https://kara.moe/karas?p=0&filter=&order=search&q=t%3A47&slug=nodamin) dont il est impossible de nier la popularité. « Ces soirées-là », « La compilation des chanteurs inconnus » ou « The Other Side of The Wall » sont des succès éternels des sessions karaokés. Mais tout ça, ce n'est rien comparé à son amour immodéré pour Aikatsu.

## Pierre Camilli

Un peu comme Nodamin, Lua, ou Nock, Pierre est toujours partant pour réaliser des karaokés rigolos quand on trouve LA vidéo qu'il nous faut absolument. Bien sûr, il fait aussi les karaokés qu'il veut, mais « Gali l'Alligator » ou « En route pour l'aventure" (oui, la pub Banga des années 80) ne seraient pas là sans lui.

## QCTX

Toujours prêt à critiquer tout ce qui bouge (et même ce qui ne bouge pas), QC' de son petit nom a toujours traîné dans nombre de projets otaku. Karaoke Mugen n'y fait pas exception : outre quelques rapports de bugs bien sentis, il est souvent de bon conseil et a notamment aidé à corriger et améliorer la documentation du logiciel.

## RdNetwork

Lorsqu'il en avait encore le temps, Rd nous a beaucoup aidé a remplir les méta-données d'une partie des karaokés pour faciliter le travail de Nock. Il a aussi réalisé quelques karaokés au passage.

## Rinchama

C'est sous l'impulsion de Rin que [le canal #karaoke s'est ouvert sur le Discord de l'Eden de la Nanami](https://karaokes.moe/discord). Ancien responsable karaoké d'Epitanime, il a apporté son savoir et une certaine rigueur au projet. C'est aussi grâce à lui que Karaoke Mugen Live existe : Rin avait alors utilisé le domaine karaokes.moe pour faire echo à [openings.moe](http://openings.moe). Nous nous servons encore aujourd'hui de ce logiciel pour faire fonctionner Live. Rin aime également beaucoup quand c'est bien rangé, et est facilement *trigger* devant un karaoké mal nommé. C'est enfin l'un de nos seuls utilisateurs sous macOS, alors on essaye d'y faire attention.

## Sedeto

Sedeto est la marraine de Karaoke Mugen. Etant une grande fan de karaoké et de chant, elle a vu le projet naître et a fourni sa mascotte, Nanamin, ainsi qu'un superbe logo. Elle est aussi toujours présente quand on a une question d'ordre graphique à poser : elle a par exemple aidé Esenjin sur les flyers récemment produits. Pour découvrir ce qu'elle fait, [allez voir son site web](http://sedeto.fr)

## spokeek

Allant et venant, Spokeek a contribué à améliorer le code de l'application Karaoke Mugen et a fourni une partie des fondations de son interface web.

## Suryan

Suryan aimerait s'impliquer plus dans Karaoke Mugen, mais le temps lui manque, alors le peu qu'il a, il l'utilise pour intégrer les karaokés des autres dans la base. De nature curieuse, il participe aussi aux discussions un peu plus techniques lorsqu'il en voit passer dans le canal. Un jour peut-être, il écrira sa première ligne de code dans l'application Karaoke Mugen ou son serveur !

## Tendoctoris

Preuve s'il en est qu'il n'y a pas besoin d'envoyer 150 karaokés pour figurer dans cette liste, Tendoctoris a grandement participé à l'élaboration de la documentation contributeur et a aussi apporté ses idées au début du projet. Aujourd'hui il se fait plus discret, mais ça ne l'empêche pas de refaire surface de temps à autre.

## Tsuchi et Il Palazzo-sama

Incroyable, pourquoi il y a deux personnes là ? Non, ils n'ont pas fusionné : mais Tsuchi et Il Palazzo sont les deux principaux gêrants de l'activité Karaoké de [Forum Thalie](http://forum-thalie.fr) et qui utilise Karaoke Mugen. Leurs retours ont été précieux depuis la 2.0 du logiciel et ils ont contribué à écrire la [documentation réservée aux opérateurs de karaoké](https://docs.karaokes.moe/fr/user-guide/operator/) pour faire une soirée kara réussie.

## Zeograd

Zeograd fait partie d'une communauté Touhou qui fait des covers de chansons tirées de cet univers, et en français s'il vous plaît ! Pour Karaoke Mugen il intègre ces chansons, et remonte bugs et autres idées pour améliorer le logiciel.

## Ziassan

Véritable magicien de l'application web aux idées parfois un peu tordues, Ziassan est l'un des pilliers de l'application Karaoke Mugen puisqu'il en a designé et codé l'interface Web de A à Z. Il aime les défis, les icônes sans texte et les blagues d'interface. Ça et puis quelques chansons un peu bizarres [qu'il vaut mieux ne pas évoquer](https://kara.moe/karas?p=0&filter=&order=search&q=t%3A29&slug=ziassan). A ses heures perdues, il fait aussi de la musique.

# Et voilà !

Comme vous avez pu le constater, le profil des membres de la communauté de Karaoke Mugen sont très divers et leurs réalisations encore plus. Chacun apporte sa pierre à l'édifice.

Je suis particulièrement fier de tout ce beau monde, et de tout ce que nous avons fait ensemble jusqu'ici. On a tendance à mettre un seul visage sur un projet, parce que c'est beaucoup plus simple pour l'imaginaire, mais Karaoke Mugen, c'est plein de petites mains qui ont aidé, et pour certaines qui aident toujours aujourd'hui.

A toutes ces personnes, celles que j'ai cité en coup de vent comme celles pour lesquelles j'ai dédié un paragraphe, j'aimerais dire merci. Merci d'avoir fait de Karaoke Mugen ce qu'il est aujourd'hui. Vos contributions m'aident à continuer à vouloir améliorer le code du programme encore plus, et à continuer d'améliorer nos procédures, notre façon de faire, à nous rendre encore plus accessibles, à écrire de la documentation, du code, et même ce post.

Merci.

Et pour les visiteurs, si vous voulez nous rejoindre et vous aussi filer un coup de main bienvenu, peu importe sur quoi : [on a plein de choses à vous faire faire](https://mugen.karaokes.moe/participate.html). Venez nous voir qu'on en parle !
