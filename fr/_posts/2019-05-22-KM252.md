---
layout: post
title:  "Karaoke Mugen 2.5.2 !"
date:   2019-05-22 06:00:00
language: fr
handle: /2019/05/22/KM252.html
---

Karaoke Mugen 2.5.2 « Konata 4-Koma » se fait belle !

![gif]({{ site.baseurl }}/images/articles/1hare89.gif){: .center-image }

Il s'agit d'une version corrective. Il restait encore quelques bugs à corriger. La 2.5.2 peut être considérée comme stable.

## Téléchargements

Comme d'hab, [direction la page de téléchargements.]({{site.baseurl}}/download.html)

## Changements

### Améliorations

- Vous pouvez maintenant forcer Karaoke Mugen à changer le mot de passe admin au lancement (au cas où vous l'auriez pas noté, ou si vous utilisez KM sur un système sans intervention humaine, comme une image Raspberry Pi ou un container Docker). Utilisez le paramètre `--forceAdminpassword <password>` au lancement, en remplaçant `<password>` par le nouveau mot de passe.

### Correctifs

- Correction du résolveur de fichiers quand on utilise plusieurs dossiers pour les karas, séries, médias ou paroles.
- Correction du mécanisme de redémarrage de mpv.
- Correction du fond d'écran qui n'apparaissait plus à la fin d'une chanson si on utilisait la fonction « arrêt après la chanson en cours ».
- Correction des tentatives de lectures supplémentaires quand mpv se plantait en voulant lire un karaoké. Cette fois il réessayera la lecture sans crasher !
- L'interface web affichera désormais la traduction anglaise si votre navigateur utilise une langue inconnue.
- Les fichiers médias ne sont plus weboptimisés si vous ne les modifiez pas dans le formulaire d'édition de karaoké.
- Les erreurs dûes aux plantages de mpv quand on passe à la chanson suivante sont maintenant correctement gérées.
- Correction (en partie) du formulaire d'édition des utilisateurs.
