---
layout: post
title:  "Karaoke Mugen 2.3.0"
date:   2018-08-14 06:00:00
language: fr
handle: /2018/08/14/KM230.html
---

Karaoke Mugen 2.3.0 « Ichika Idolâtrice » est sortie ! Pour la version 2.3.x nous utiliserons le nom d'Ichika de l'animé « Ano Natsu de Matteru ».

![gif]({{ site.baseurl }}/images/articles/1hz98rh.gif){: .center-image }

Rappels utiles :

- Les changements entre la Release Candidate 1 et cette version finale seront marqués en gras. Parce que le gras, c'est la vie.
- Vu qu'il y a souvent des changements structurels de base de données entre des versions majeures, on vous conseille vivement **de [régénérer votre base de données de karaokés](https://docs.karaokes.moe/fr/contrib-guide/manage/#generer-la-base-de-donnees)** lors de votre première utilisation de cette version.

## Téléchargements

Comme d'hab, [direction la page de téléchargements]({{site.baseurl}}/download.html)

### Nouvelles fonctionnalités innovantes(tm)

- #118 Vous pouvez désormais générer des fichiers .kara dans le panneau de contrôle de Karaoke Mugen via un formulaire à remplir. Cela vous permettra de participer plus facilement à la base de karaokés puisque vous n'avez plus de fichiers à modifier manuellement. Indiquez votre vidéo, votre fichier karaoké, et générez !
- #325 Un bouton vous permet désormais de suggérer des karaokés qui n'existeraient pas encore dans la base de Karaoke Mugen. Ceci enverra un mail et créera une issue automatiquement dans [le Gitlab de l'équipe](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=suggestion).
- #340 La liste des karaokés peut être ordonnée par nouveauté. Cela vous permet de voir quels sont les nouveaux karaokés de la base !
- #120 Un nouveau mode de navigation a été ajouté, vous permettant de visualiser les chansons par tag, par langue, chanteur/euse, mais aussi par série ou année! **Tous les karaokés n'ont pas leurs informations remplies : aidez-nous à compléter les métadonnées!**
- #305 Une fonction de mélange plus intelligente est disponible pour ceux qui ont des grosses playlists :
  - Elle évite que les chansons trop longues soient trop proches.
  - Les chansons ajoutées par un utilisateur ne se suivront plus forcément et seront étalées au sein de la playlist
- #334 La base de données des séries peut être modifiée depuis l'interface du panneau de contrôle. Cela met à jour le fichier `series.json` ainsi que les .kara impactés.
- #324 Les opérateurs de karaoké peuvent maintenant libérer les chansons manuellement.
- #153 Un lien « Plus d'informations » a été ajouté au panneau d'informations d'un karaoké. Il vous permet d'en savoir plus sur une série ou chanteur/euse !
- #152 Via une option, vous pouvez maintenant ajouter plusieurs fois la même chanson dans la playlist courante !

### Améliorations

- #336 L'interface web sera dorénavant grisée si Karaoke Mugen est arrêté.
- #330 Les boutons ont été normalisés au sein de l'interface.
- #322 Beaucoup d'optimisations ont été faites au code de Karaoke Mugen, le rendant plus rapide, plus simple à lire, et rendant son poil soyeux et ses couleurs chatoyantes.
- #321 Le dossier temporaire est nettoyé au démarrage
- #320 L'heure de dernière action d'un utilisateur est temporisée avant d'être écrite en base.
- Le fichier `userdata.sqlite3` est sauvegardé avant chaque génération de la base.
- #139 Le curseur pour la taille de la fenêtre de MPV affiche désormais le pourcentage.
- **Le bouton « Update from Shelter » renvoie désormais un message immédiatement, vous invitant à consulter la console de Karaoke Mugen pour suivre la progression de la mise à jour.**
- **Le message « Connexion perdue » affiche maintenant un effet de neige/statique.**
- **La base de données est maintenant plus optimisée, rendant les opérations sur les playlists beaucoup plus rapides.**

### Correctifs

- #326 Les chansons ne peuvent plus être ajoutées si elles se trouvent dans la blacklist.
- #317 Quand la base de données est en maintenance, les erreurs SQLITE_BUSY n'apparaitront plus.
- Le moteur demande si le lecteur vidéo est prêt avant d'accepter des commandes.
- **#328 La barre de progression durant les mises à jour devrait maintenant s'afficher correctement selon la largeur de la fenêtre et ne plus afficher « Infinity ».
- **Le panneau des filtres d'affichage de la liste de karaokés devrait maintenant s'afficher normalement sous Safari iOS.**
- **Le fichier de configuration ne devrait plus être écrasé malencontreusement au démarrage de l'application (normalement).**
- **Correction de l'affichage des séries, des tags et des listes de karaokés dans le panneau de contrôle.**
- **Correction du bouton « Arrêt après la chanson en cours ».**

Si vous avez des questions, ou besoin d'aide, n'hésitez pas à [nous contacter]({{site.baseurl}}/contact.html) !
