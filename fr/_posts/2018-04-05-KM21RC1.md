---
layout: post
title:  "Karaoke Mugen 2.1 Release Candidate 1"
date:   2018-04-05 01:00:00
language: fr
handle: /2018/04/05/KM21RC1.html
---

Nous mettons à disposition la première release candidate de la tant attendue version 2.1 !

# Bugs connus

- La mise à jour du logiciel via l'interface est pour le moment désactivée à cause de certains bugs à corriger pour la sortie finale.

# Changements

La liste des changements sont pour le moment en anglais, mais une version française sera bientôt disponible avec la sortie officielle de la 2.1.

[Voir la liste des changements](https://gitlab.com/karaokemugen/code/karaokemugen-app/blob/6c979702e01764c4326dec007dedad43987f620d/CHANGELOG.md)

# Téléchargements

Direction [la page des téléchargements !]({{site.baseurl}}/download.html)