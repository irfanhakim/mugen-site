---
layout: post
title:  "La 2.0 est sortie, et ensuite ?"
date:   2017-11-10 10:00:00
language: fr
handle: /2017/11/10/roadmap.html
---

Ce billet a pour but de vous expliquer ce qui est prévu ensuite pour **Karaoke Mugen**. Car la sortie de la 2.0 ne signifie pas que tout est fini, bien au contraire.

Nous sommes toujours à l'affut de personnes souhaitant aider au projet dans sa globalité, et il est je pense plus facile de voir si vous pouvez aider si vous connaissez nos ambitions (spoiler: c'est la domination du monde par le karaoke).

# Karaoke Mugen 2.1 « Gabriel Glamoureuse »

La Gabriel est bien sûr celle de Gabriel Dropout.

![gabi]({{ site.baseurl }}/images/articles/gabriel-dropout.jpg){: .max-image }

La plus grosse nouveauté de **Karaoke Mugen** sera la gestion des utilisateurs. Aujourd'hui on ne différencie les utilisateurs que par le pseudo, mais ce n'est pas très fiable : n'importe qui peut changer de pseudo à tout moment. L'idée est donc de proposer un système d'authentification par login/mot de passe basique mais efficace. Pourquoi basique ? Parce qu'on ne veut pas rendre l'utilisation de **Karaoke Mugen** inutilement complexe.

L'intêret de ce système est multiple :

- Pouvoir proposer un profil utilisateur, avec par exemple la liste de ses karaokes les plus demandés, mais surtout la possibilité pour lui de mettre des karas en favoris pour les retrouver plus facilement.
- Pouvoir ajouter des modes de selection de karaoke : vote du public, « liker » des suggestions pour aider l'admin à choisir, etc. Il faut bien comprendre que de nombreuses fonctionnalités sont impossibles à créer tant qu'on aura pas fait ce système d'utilisateurs.

D'autres fonctionnalités sont prévues pour améliorer le logiciel, vous pouvez les suivre sur [la page de la milestone 2.1](https://gitlab.com/karaokemugen/code/karaokemugen-app/milestones/2) sur le lab.

Pour tout cela, on aurait besoin de développeurs voulant faire du NodeJS ou du développement frontend en JS également. Notez que si vous débutez en JS, c'est pas grave, on est là pour apprendre et s'y mettre. **Karaoke Mugen est un excellent projet pour se mettre à apprendre un nouveau langage !** Moi même je ne connaissais pas NodeJS il y a six mois.

# Jingle Bells

Vous l'avez peut-être remarqué si vous utilisez **Karaoke Mugen** mais on a de jolis petits jingles qui passent de temps en temps entre les chansons. C'est un petit délire qu'on a eu un soir, et Nock a cru bon de nous suivre : en reprenant certains *eyecatch* de séries connues et en les modifiant pour y afficher le nom de l'application, ça faisait une jolie petite pause entre les karaokes, et ça permettait également d'afficher les informations de connexion au public un peu plus longtemps.

![jingle]({{ site.baseurl }}/images/articles/hetht.png){: .max-image }

On a une petite dizaine de jingles, mais évidemment, on en fait très vite le tour ! Si vous pensez pouvoir en créer de nouveaux, on a des *eyecatch* tout prêts à disposition qu'il faudrait modifier aux couleurs de **Karaoke Mugen**.

À noter également qu'on a eu l'idée de faire des « moment sponsors » avec de faux sponsors. Vous savez, ces petites pubs qu'on entend après le générique d'ouverture d'un animé : *kono bangumi wa goran no suponsaa no teikyou de okurishimasu*.

Si aider en réalisant ces courts clips vous intéresse, alors soyez les bienvenus, parce qu'on ne peut pas tout faire :)

# IN THE DATABASE, DATABASE, WOW WOW

La base de données de karaokes est bien sûr toujours disponible sur son dépôt Git. Tout le monde peut donc y proposer des modifications, et ça tombe bien car on a vraiment besoin de l'améliorer.

Chaque karaoke contient pas mal de métadonnées, comme l'année de production, le studio responsable de l'animé, le chanteur, le compositeur, ou encore la langue, tout simplement. Ces données sont importantes car elles permettent de faire des recherches depuis l'interface et d'aider pour les filtres.

Remplir tout cela prend du temps mais ce n'est pas une tâche difficile, il faut juste savoir faire quelques recherches sur Internet, la plupart du temps on trouve, et si on ne trouve pas, ce n'est pas si grave !

En outre il y a aussi :

- [Les karaokes mal timés.](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=mal+tim%C3%A9) Il y en a en effet un petit paquet à « réparer » et sûrement d'autres qu'on a pas encore remarqués. Remonter des problèmes nous aide aussi énormément.
- [Les karaokes avec des vidéos de mauvaise qualité.](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=mauvaise+qualit%C3%A9) Comme la base existe depuis de nombreuses années, il est important de renouveller parfois la qualité des vidéos... cela prend du temps, surtout qu'il faut parfois recaler correctement les paroles pour coller à la nouvelle vidéo !
- [Les karaokes suggérés.](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=suggestion) Parfois on écoute un truc et on se dit que ça ferait un joli kara, ou bien simplement parce qu'on aime beaucoup la chanson. Du coup hé bien, il faut écrire la version karaoke, et ça prend un peu de temps !

On doit également passer les titres, les noms de série ainsi que les types de chansons à l'intérieur des fichiers .kara pour éviter d'avoir à les lire depuis le nom du fichier. Pour quelqu'un s'y connaissant avec les outils libres tels que `grep`, ou `sed` c'est assez trivial, mais il faut prendre le temps de le faire.

Tout comme normaliser les karaokes, les *dékikoololiser* pour certains, même...

# S'occuper du site web et de la documentation

Vous le voyez ici, le site web est assez basique, notamment parce qu'on a pas forcément le temps de s'en occuper non plus. On aimerait bien mettre mieux en avant **Karaoke Mugen**, ou, par exemple [réaliser une FAQ pour les questions les plus fréquentes](https://gitlab.com/karaokemugen/sites/mugen-site/issues/15). On aimerait égaleemnt fusionner [le site de documentation](https://docs.karaokes.moe/) et [celui sur lequel vous vous trouvez](https://mugen.karaokes.moe) mais il faut savoir se servir de Jekyll et MkDocs...

# Et j'en oublie sûrement !

[Gitlab](https://gitlab.com/karaokemugen) nous sert énormément pour garder une trace de notre avancement et gérer les choses à faire. Ce n'est pas optimal, mais c'est primordial pour avancer tranquillement.

# Et vous ?

C'est pas facile de demander de l'aide, mais plus je vois la montagne de trucs à faire, plus je me dis qu'on a vraiment besoin de gens motivés pour aider à maintenir la base, s'occuper des sites et bien sûr, développer. Même des idées, bonnes ou mauvaises, sont les bienvenues pour faire avancer tout ça.

![vous]({{ site.baseurl }}/images/articles/546dc444.png){: .center-image }

Ce billet avait surtout pour but de faire un point d'étape mais je réalise qu'en fait, ça ressemble plutôt à un appel à l'aide. **Karaoke Mugen** va avancer à son rythme avec les moyens qu'on a, ça va prendre beaucoup de temps, mais ça va arriver. Bien sûr, ça arrivera plus vite encore si vous nous aidez !
